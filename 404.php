<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

			<main id="main" class="medium-6 medium-centered large-6 large-centered columns" role="main">

				<article id="content-not-found">

					<header class="article-header">

						<h1 class="text-center"><?php _e( 'Oops! - Something went wrong.', 'Frameweworque' ); ?></h1>

					</header>

					<section class="entry-content">

						<p class="text-center">

							<?php _e( 'Did you check our accessories section already?', 'Frameworque' ); ?>
							test

						</p>

					</section>


<!--
					<section class="search">

					    <p><?php get_search_form(); ?></p>

					</section>
-->

				</article>

			</main>

			<div class="row">

				<div class="medium-12 large-10 large-centered columns">

					<?php echo do_shortcode('[product_category category="accessoires" per_page="6" columns="3" orderby="menu_order" order="asc"]'); ?>

				</div>

			</div>

		</div>

	</div>

<?php get_footer(); ?>