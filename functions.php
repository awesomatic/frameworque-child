<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, filemtime(get_template_directory_uri() . '/style.css' ));
    wp_enqueue_style( 'child-style',
        filemtime (get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )),
        wp_get_theme()->get('Version')
    );
}

// enqueue styles for child theme
function example_enqueue_styles() {

	// enqueue parent styles
	wp_enqueue_style('overrides-css', get_stylesheet_directory_uri() .'/overrides.css');

}
add_action('wp_enqueue_scripts', 'example_enqueue_styles');


/**
 * Jetpack Compatibility File
 *
 * @link https://www.advancedcustomfields.com/resources/acf_add_options_page/
 *
 * @package Jordi_Radstake
 */

function my_acf_init() {

	if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
    'icon_url' => 'dashicons-megaphone',
		'redirect'		=> false
	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Header Settings',
	// 	'menu_title'	=> 'Header',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
  //
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Footer Settings',
	// 	'menu_title'	=> 'Footer',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));

	}

}

//add_action('acf/init', 'my_acf_init');


// add_action( 'woocommerce_after_shop_loop_item_title', 'custom_before_title' );
// function custom_before_title() {
//
//     global $product;
//
//     echo "<span class='price-icon'>|<span>";
//
// }
